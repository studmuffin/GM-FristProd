/// @description Insert description here
// You can write your code in this editor
if !overlord.finished {
	if keyboard_check(vk_left) {
		var _inst = instance_place(x-4, y, wall1);
		if _inst == noone {
			x -= 4;
			walking=true;
		}
	} 
	if keyboard_check(vk_right) {
		var _inst = instance_place(x+4, y, wall1);
		if _inst == noone {
			x += 4;
			walking=true;
		}
	}

	if keyboard_check(vk_up) {
		var _inst = instance_place(x, y-4, wall1);
		if _inst == noone {
			y -= 4;
			walking=true;
		}
	}

	if keyboard_check(vk_down) {
		var _inst = instance_place(x, y+4, wall1);
		if _inst == noone {
			y += 4;
			walking=true;
		}
	}
	walkingtimer-=1;
	step2_script();
	pick_treasure0();
}