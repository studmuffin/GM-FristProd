{
    "id": "24cf8b14-35b2-4795-af81-88e075e97adb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player2",
    "eventList": [
        {
            "id": "9beb7267-4256-4b0e-825b-3cb4315d62d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24cf8b14-35b2-4795-af81-88e075e97adb"
        },
        {
            "id": "b55682cc-bad9-45ea-908f-efc764751d54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24cf8b14-35b2-4795-af81-88e075e97adb"
        },
        {
            "id": "ecd140b6-f020-45ba-981d-c6babb998c7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "24cf8b14-35b2-4795-af81-88e075e97adb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "b76c5a76-1622-4d4c-9393-3e4ff2a14869",
    "visible": true
}