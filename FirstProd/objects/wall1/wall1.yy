{
    "id": "e155b6b9-7f7d-4828-9331-778b68009faf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "wall1",
    "eventList": [
        {
            "id": "134ab910-9c7f-4544-a101-9868019359b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e155b6b9-7f7d-4828-9331-778b68009faf"
        },
        {
            "id": "3cd2e37d-8b4c-4b21-9b8d-2680c631dd8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e155b6b9-7f7d-4828-9331-778b68009faf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c9204f6b-995b-4dca-a4e7-d36a89c0a39d",
    "visible": true
}