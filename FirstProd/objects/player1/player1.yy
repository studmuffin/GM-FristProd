{
    "id": "77854866-0dbd-499f-87cf-0680adc0c3b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player1",
    "eventList": [
        {
            "id": "fdfe2582-c09d-437b-bb19-4cd9c6fa2cc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77854866-0dbd-499f-87cf-0680adc0c3b3"
        },
        {
            "id": "c211400a-23ed-4512-8add-ccb33532f6b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77854866-0dbd-499f-87cf-0680adc0c3b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "b76c5a76-1622-4d4c-9393-3e4ff2a14869",
    "visible": true
}