{
    "id": "027f01d1-ca63-47ee-88c4-f8143f7287de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "treasure1",
    "eventList": [
        {
            "id": "d14ccc41-6630-4163-930b-94abf8b85806",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "027f01d1-ca63-47ee-88c4-f8143f7287de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b2aeeee-b3a7-45b0-911c-bdbe0d6878b1",
    "visible": true
}