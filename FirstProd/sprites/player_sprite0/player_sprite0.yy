{
    "id": "b76c5a76-1622-4d4c-9393-3e4ff2a14869",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb5dc301-8a69-4d3b-8864-6df9ac36ac5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76c5a76-1622-4d4c-9393-3e4ff2a14869",
            "compositeImage": {
                "id": "9fd1e91e-b587-41a3-adf4-d570fa1371de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb5dc301-8a69-4d3b-8864-6df9ac36ac5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30f03961-d3ec-4674-bb3f-83978b993c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5dc301-8a69-4d3b-8864-6df9ac36ac5e",
                    "LayerId": "d1371d06-5445-46e8-ae39-fc831f04fc4f"
                }
            ]
        },
        {
            "id": "3528ea5f-ebc0-4fc2-a33d-acf2c1c62a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76c5a76-1622-4d4c-9393-3e4ff2a14869",
            "compositeImage": {
                "id": "c3a524b2-b568-4602-8c21-6a2bd9f3b71c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3528ea5f-ebc0-4fc2-a33d-acf2c1c62a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c538e169-d68f-469a-ae31-50b0a41da8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3528ea5f-ebc0-4fc2-a33d-acf2c1c62a75",
                    "LayerId": "d1371d06-5445-46e8-ae39-fc831f04fc4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d1371d06-5445-46e8-ae39-fc831f04fc4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b76c5a76-1622-4d4c-9393-3e4ff2a14869",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 29,
    "yorig": 21
}