{
    "id": "8b2aeeee-b3a7-45b0-911c-bdbe0d6878b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "treasure1_sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 7,
    "bbox_right": 57,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45b6ddcd-25f0-40c6-95a7-d3715157839d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b2aeeee-b3a7-45b0-911c-bdbe0d6878b1",
            "compositeImage": {
                "id": "3158c0d4-3cc2-430d-8a0a-598458ef7c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b6ddcd-25f0-40c6-95a7-d3715157839d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abacdd64-1bd0-4e7c-befa-3e21912515a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b6ddcd-25f0-40c6-95a7-d3715157839d",
                    "LayerId": "2049666f-2b73-4a24-899f-2a0c50d42246"
                }
            ]
        },
        {
            "id": "dddfb110-1b30-44d3-bbf0-323258cb383e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b2aeeee-b3a7-45b0-911c-bdbe0d6878b1",
            "compositeImage": {
                "id": "e09da818-b647-40ea-8d72-0104c788b566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dddfb110-1b30-44d3-bbf0-323258cb383e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fdc270d-92ad-4bac-baaf-0033c0a072e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dddfb110-1b30-44d3-bbf0-323258cb383e",
                    "LayerId": "2049666f-2b73-4a24-899f-2a0c50d42246"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2049666f-2b73-4a24-899f-2a0c50d42246",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b2aeeee-b3a7-45b0-911c-bdbe0d6878b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}