{
    "id": "c9204f6b-995b-4dca-a4e7-d36a89c0a39d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wall_sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "325a3aa3-fa17-48fc-b20b-467021a15237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9204f6b-995b-4dca-a4e7-d36a89c0a39d",
            "compositeImage": {
                "id": "082778ae-58dc-49e8-b8b9-e5ed81bc32cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "325a3aa3-fa17-48fc-b20b-467021a15237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d2bf30-fca9-43d2-b050-629a73d4764f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "325a3aa3-fa17-48fc-b20b-467021a15237",
                    "LayerId": "90951adc-2348-4bfb-a74b-a671333d8a61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "90951adc-2348-4bfb-a74b-a671333d8a61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9204f6b-995b-4dca-a4e7-d36a89c0a39d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}